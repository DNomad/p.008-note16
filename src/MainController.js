var MainController = (function(){

	var that = {},
		beatCountSelection = 1,
		modulesLoaded = 0,

	// Initialisiert den MainController
	init = function(){
		IntroView.init();
		_registerListeners();
	},

	// Registriert alle Listener
	_registerListeners = function(){
		$("body").on("PRELOAD_START", _initNote16);
		$("body").on("MODULE_FINISHED", _checkModules);
	},

	// Initialisiert die gesamte Anwendung
	_initNote16 = function(event, data){
		beatCountSelection = data.count;
		var project = null;
		try{
			project = JSON.parse(data.import);
		}catch(e){
			console.log(e);
		}
		if(project && _checkProjectStructure(project)){
			console.log("imported");
		}else{
			console.log("generating");
			project = _generateBlankProject();
		}
		UIController.init(project.beatCount);
		NoteEngine.init(project);
	},

	// Überprüft, ob alle notwendigen Module geladen wurden
	_checkModules = function(){
		modulesLoaded++;
		if(modulesLoaded == 8){
			$("#loadingDialog").toggle();
		}
	},

	// Überprüft, ob das geparste Objekt der notwendigen Struktur 
	// für einen Import entspricht
	_checkProjectStructure = function(obj){
		if(!obj.beatCount 
			|| !obj.rhythmID 
			|| !obj.bpm
			|| !obj.stats) return false;

		if(jQuery.type(obj.rhythmID) !== "string" 
			|| jQuery.type(obj.bpm) !== "number"
			|| jQuery.type(obj.beatCount) !== "number"
			|| jQuery.type(obj.stats) !== "array") return false;

		var stats = obj.stats;
		for(var i in stats){
			var temp = stats[i];
			if(jQuery.type(temp.instrument) !== "string" 
				|| jQuery.type(temp.stats) !== "array") return false;
			try{
				var arr = temp.stats;
				for(var i = 0; i < arr.length; i++){
					for(var j = 0; j < arr[i].length; j++){
						if(jQuery.type(arr[i][j]) !== "number") return false;
					}
				}
			}catch(e){
				console.log(e);
				return false;
			}
		}

		return true;
	},

	// Generiert ein leeres Projekt
	_generateBlankProject = function(){
		var result = {};
		result.beatCount = beatCountSelection;
		result.rhythmID = "rhythm0";
		result.bpm = 100;
		var arr = [];
		for(var i = 0; i < 16; i++){
			arr[i] = [];
			for(var j = 0; j < (16 * beatCountSelection); j++){
				arr[i][j] = 0;
			}
		}
		result.stats = [
			{instrument: "piano", stats: arr},
			{instrument: "pizzicato", stats: arr},
			{instrument: "bass", stats: arr},
			{instrument: "celesta", stats: arr}
		];

		return result;
	};
	
	that.init = init;
	return that;
})();