var TabView = (function(){
	
	var that = {},
		mainContainer = null,
		tabCount,

	init = function(barSelection){
		mainContainer = document.querySelector("#matrixContentContainer");
		tabCount = barSelection;
		_generateHorizontalTabs();
	},

	// Generiert alle oberen Takttabs
	// die Zeitleiste
	// und die Container für die Subtabs
	_generateHorizontalTabs = function(){
		var horizontalTabContainer = document.createElement("ul"),
			horizontalTabContentContainer = document.createElement("div"),
			timeLineContainer = document.createElement("div");

		horizontalTabContainer.classList.add("nav");
		horizontalTabContainer.classList.add("topTab");
		horizontalTabContainer.classList.add("nav-tabs");
		horizontalTabContainer.classList.add("nav-justified");
		horizontalTabContentContainer.classList.add("tab-content");
		horizontalTabContentContainer.classList.add("tab-content-horizontal");
		timeLineContainer.classList.add("timeLine");

		for(var i = 0; i < tabCount; i++){
			var tab = document.createElement("li"),
				tabText = document.createElement("a"),
				beatTabContent = document.createElement("div"),
				timeLineTab = document.createElement("span");

			beatTabContent.classList.add("tab-pane");
			beatTabContent.id = "beat" + i;
			timeLineTab.classList.add("timeLineTab");
			timeLineTab.style.width = _calcProperTimelineTabWith(tabCount);
			timeLineTab.id = "timeLine" + i;

			if(i == 0){
				tab.classList.add("active");
				beatTabContent.classList.add("active");
				timeLineTab.classList.add("current");
			}

			tabText.innerHTML = "Beat " + (i+1); 
			tabText.setAttribute("href", "#beat" + i);
			tabText.setAttribute("data-toggle", "tab");
			tabText.classList.add("tabText");

			tab.id = "beatTab" + i;
			tab.appendChild(tabText);

			horizontalTabContainer.appendChild(tab);
			timeLineContainer.appendChild(timeLineTab);
			var instrumentTabs = _generateSubTabs("beat" + i);
			beatTabContent.appendChild(instrumentTabs);
			horizontalTabContentContainer.appendChild(beatTabContent);
		}
		if(tabCount > 1){
			mainContainer.appendChild(horizontalTabContainer);
			mainContainer.appendChild(timeLineContainer);
		}else{
			$("#mainContentContainer").css("margin-top", "3%");
		}
		mainContainer.appendChild(horizontalTabContentContainer);
		$("body").trigger("TABVIEW_FINISHED");
	},

	// Generiert alle Untertabs für einen Takt
	// sowie die Container für alle Matrizen
	_generateSubTabs = function(id){
		var finalContainer = document.createElement("div"),
			tabContainer = document.createElement("ul"),
			contentContainer = document.createElement("div");

		finalContainer.classList.add("tabbable");
		finalContainer.classList.add("tabs-left");
		finalContainer.classList.add("tab-links");
		tabContainer.classList.add("nav");
		tabContainer.classList.add("nav-tabs");
		contentContainer.classList.add("tab-content");
		contentContainer.classList.add("tab-content-vertical");

		for(var i = 0; i < 4; i++){
			var tab = document.createElement("li"),
				tabText = document.createElement("a"),
				content = document.createElement("div");

			if(i == 0){
				tab.classList.add("active");
				content.classList.add("active");
			}

			tab.classList.add(_getInstrumentText(i));
			tabText.classList.add("tabTextVertical");
			tabText.setAttribute("href", "." + _getInstrumentText(i));
			tabText.setAttribute("data-toggle", "tab");
			tabText.innerHTML = _getSubtabName(i);
			content.classList.add("tab-pane");
			content.id = "" + id + _getInstrumentText(i);
			content.classList.add(_getInstrumentText(i));
			tab.appendChild(tabText);
			tabContainer.appendChild(tab);
			contentContainer.appendChild(content);
		}

		finalContainer.appendChild(tabContainer);
		finalContainer.appendChild(contentContainer);
		return finalContainer;
	},

	// Gibt den Namen des Instruments je nach Index zurück
	_getInstrumentText = function(index){
		switch(index){
			case 0:
				return "piano";
			case 1:
				return "pizzicato";
			case 2:
				return "bass";
			case 3:
				return "celesta";
			default:
				return "piano";
		}
	},

	_getSubtabName = function(index){
		switch(index){
			case 0:
				return "Piano";
			case 1:
				return "Pizzicato";
			case 2:
				return "Bass";
			case 3:
				return "Celesta";
			default:
				return "Piano";
		}
	}

	// Berechnet die Breite der Timeline Tabs je nach Anzahl der Takte
	// die übergeben wurde
	_calcProperTimelineTabWith = function(count){
		switch(count){
			case 1:
				return "99%";
			case 2:
				return "49%";
			case 4:
				return "24%";
			default:
				return "24%";
		}
	};

	that.init = init;
	return that;
})();